# Introduction Thebban
# Hello Everyone!

## My Name is Thebban 
But you can call me Ben, if one more syllable is hard for you.  
Here is what I look like :-

![Here is what I look like](https://media-exp1.licdn.com/dms/image/C5603AQFjJYtl6JlOPQ/profile-displayphoto-shrink_200_200/0/1617094258201?e=1640217600&v=beta&t=WIoSAUnGQk5iKKhWpbOLXTPcP70EAze5qnxEhFcUY_I)  
So like all humans I have my strengths and weaknesses, some of which you could see in the table below:-  
| Strength | Weakness |
| -------- | -------- |
| Versatile | Disorganized |
| Speed | Strength |
| Respectful | Competitive |  

I was born in Selangor for some reason but I have lived in Johor for practically my whole life, in a little town called Nusa Bestari.  
  
  I guess that would make me a child of two states. Enough about where I was from, here are some of the things i like doing in my spare time. :-  
  1. Playing Football 
2. Watching Movies under Film Festivals 
3. Going through reddit for *dem memez*   
  
I guess to wrap it up, here is my favourite quote ;-  
> I thought to myself with what means, with what deceptions, with how many varied arts, with what industry a man sharpens his wit to deceive another
, and through these variations the world is made more beautiful.  

*-Francesco Vettori,Early Sixteenth Century*
